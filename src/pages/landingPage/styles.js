import styled from 'styled-components';

const Span = styled.span`
  font-weight: bold;
`;

export { Span as default, Span };
