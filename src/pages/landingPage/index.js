import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Grid, Typography } from '@material-ui/core';
import { Creators as BooksActions } from '../../ducks/stores/books';
import { Card, Modal } from '../../components';
import { formatDate } from '../../helper/utils';
import { Span } from './styles';

const LandingPage = () => {
  const dispatch = useDispatch();
  const [modal, setModal] = useState(false);
  const [valuesModal, setValuesModal] = useState({});

  useEffect(() => {
    dispatch(BooksActions.getAllBooks());
  }, [dispatch]);

  const handleClickCard = values => {
    setValuesModal(values);
    setModal(true);
  };

  const getAllBooks = useSelector(state => state.books.payload);
  return (
    <>
      {modal && (
        <Modal
          open={modal}
          close={() => setModal(false)}
          content={
            <>
              <p>
                <Span>Date:</Span>
                {formatDate(valuesModal.publishedDate)}
              </p>
              <p>
                <Span>Description: </Span>
                {valuesModal.description}
              </p>
              <p>
                <Span>Pages:</Span> {valuesModal.pageCount}
              </p>
              <p>
                {valuesModal.authors.length > 1 ? <Span>Autores: </Span> : <Span>Autor: </Span>}
                {valuesModal.authors.join(', ')}
              </p>
            </>
          }
        />
      )}
      <Grid container justify="center" alignItems="flex-start" style={{ marginTop: '40px' }}>
        <Grid container item xs={12} md={12} justify="center" alignItems="flex-start">
          <Typography variant="h4" component="h4">
            Books
          </Typography>
        </Grid>
        <Grid container item xs={12} md={12} justify="center" alignItems="flex-start">
          <Typography>
            Loreim ipsum sit amet, consectetur adipiscing elit. Praesent vitae eros eget telius
            trisquine bibendeum. Donec rutrum sed sem quis venenatis
          </Typography>
        </Grid>

        <Grid container justify="center" alignItems="flex-start" style={{ margin: '40px 0px' }}>
          <Grid container item xs={5} md={5}>
            {getAllBooks?.data?.map(({ title, smallThumbnail, ...rest }) => {
              return (
                <Card
                  key={title}
                  onClick={() => handleClickCard(rest)}
                  image={smallThumbnail}
                  title={title}
                />
              );
            })}
          </Grid>
        </Grid>
      </Grid>
    </>
  );
};

export default LandingPage;
