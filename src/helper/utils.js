const formatDate = date => {
  if (typeof date !== 'undefined' && date.length > 4) {
    const day = date.slice(8, 10);
    const month = date.slice(5, 7);
    const year = date.slice(0, 4);
    return `${month}/${day}/${year}`;
  }
  return date;
};

export { formatDate as default, formatDate };
