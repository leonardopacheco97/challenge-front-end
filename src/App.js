import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { Provider } from 'react-redux';
import store from './ducks/stores';
import LandingPage from './pages/landingPage';
import { Header, Footer } from './components';

function App() {
  return (
    <Provider store={store}>
      <BrowserRouter>
        <Header />
        <Switch>
          <Route path="/" component={LandingPage} exact />
        </Switch>
        <Footer />
      </BrowserRouter>
    </Provider>
  );
}

export default App;
