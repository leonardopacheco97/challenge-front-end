import { createActions, createReducer } from 'reduxsauce';

/**
 * @description Action types & creators
 */
export const { Types, Creators } = createActions({
  getAllBooks: [],
  successBooksAll: ['payload'],
});

/**
 * @description Handlers
 */
const INITIAL_STATE = {
  payload: [],
};

const getAllBooks = (state = INITIAL_STATE) => ({
  ...state,
});

const successBooksAll = (state = INITIAL_STATE, action) => ({
  ...state,
  payload: action.payload,
});

const HANDLERS = {
  [Types.GET_ALL_BOOKS]: getAllBooks,
  [Types.SUCCESS_BOOKS_ALL]: successBooksAll,
};

/**
 * @description Reducer
 */
export default createReducer(INITIAL_STATE, HANDLERS);
