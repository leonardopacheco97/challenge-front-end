const responseGetAllBooks = r => {
  const response = {
    data: r.data?.items.map(
      ({
        volumeInfo: { authors, publishedDate, description, pageCount, title, imageLinks = {} },
      }) => ({
        publishedDate,
        description,
        pageCount,
        authors,
        title,
        smallThumbnail: imageLinks && imageLinks.smallThumbnail,
      })
    ),
  };
  return response;
};

export { responseGetAllBooks as default, responseGetAllBooks };
