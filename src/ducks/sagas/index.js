import { all } from 'redux-saga/effects';
import { watchBooksActions } from './books';

function* rootSaga() {
  yield all([watchBooksActions()]);
}

export { rootSaga as default, rootSaga };
