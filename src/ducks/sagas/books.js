import { takeEvery, put } from 'redux-saga/effects';
import axios from 'axios';
import { Types as BooksTypes, Creators as BooksActions } from '../stores/books';
import { urlBooks } from '../model/endpoint';
import { responseGetAllBooks } from '../model/response/books';

function* getAllBooks() {
  const response = yield axios.get(urlBooks);
  yield put(BooksActions.successBooksAll(responseGetAllBooks(response)));
}

function* watchBooksActions() {
  yield takeEvery(BooksTypes.GET_ALL_BOOKS, getAllBooks);
}

export { watchBooksActions as default, watchBooksActions };
