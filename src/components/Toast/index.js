/* eslint-disable no-nested-ternary */
import React from 'react';
import PropTypes from 'prop-types';
import { Snackbar } from '@material-ui/core';
import MuiAlert from '@material-ui/lab/Alert';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import CancelIcon from '@material-ui/icons/Cancel';
import ErrorIcon from '@material-ui/icons/Error';
import { useStyles } from './styles';

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const Toast = ({ open, close, message, duration, type }) => {
  const classes = useStyles();

  return (
    <Snackbar
      open={open}
      autoHideDuration={duration}
      onClose={() => close()}
      anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
      className={classes.root}
    >
      <Alert
        onClose={() => close()}
        severity={type}
        className={classes.alert}
        icon={
          type === 'success' ? (
            <CheckCircleIcon />
          ) : type === 'error' ? (
            <CancelIcon />
          ) : (
            <ErrorIcon />
          )
        }
      >
        <strong>{type === 'error' ? 'Error' : type === 'success' ? 'success' : 'Attention'}</strong>
        {message}
      </Alert>
    </Snackbar>
  );
};

Toast.propTypes = {
  open: PropTypes.bool,
  close: PropTypes.func.isRequired,
  message: PropTypes.string,
  duration: PropTypes.number.isRequired,
  type: PropTypes.string.isRequired,
};

Toast.defaultProps = {
  open: false,
  message: null,
};

export default Toast;
