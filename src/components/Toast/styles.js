import { makeStyles } from '@material-ui/styles';

const useStyles = makeStyles(() => ({
  root: {
    '& strong': {
      color: '#3D3E43',
      fontWeight: '600',
      fontSize: 14,
    },
    '& .MuiAlert-filledSuccess': {
      background: '#F1F9F6',
      border: 'solid 1px #5AB38C',
      color: '#3D3E43',
      fontSize: 14,
      fontWeight: '400',
      '& .MuiAlert-icon, & .MuiAlert-action': {
        color: '#19B98E',
      },
    },
    '& .MuiAlert-filledError': {
      background: '#FFEAEA',
      border: 'solid 1px #E65750',
      color: '#3D3E43',
      fontSize: 14,
      fontWeight: '400',
      '& .MuiAlert-icon, & .MuiAlert-action': {
        color: '#FA3F3F',
      },
    },
  },
  alert: { borderRadius: '8px' },
}));

export { useStyles as default, useStyles };
