import { memo } from 'react';
import { withStyles, TextField } from '@material-ui/core';

const CssTextField = withStyles({
  root: {
    fontFamily: 'roboto',
    position: 'relative',
    textAlign: 'right',
    '& .MuiInputBase-input': {
      paddingRight: 36,
      fontSize: 14,
      fontFamily: 'roboto',
      '&::placeholder': {
        fontFamily: 'roboto',
      },
    },
    '& .MuiFormHelperText-root.Mui-error': {
      fontWeight: 600,
      backgroundColor: 'black',
      margin: 0,
      paddingLeft: 14,
      color: 'red',
      fontFamily: 'roboto',
    },
    '& .MuiInput-underline': {
      borderBottomColor: 'blue',
    },
    '& .MuiOutlinedInput-root': {
      '& fieldset': {
        borderColor: 'blue',
      },
      '&:hover fieldset': {
        borderColor: 'blue',
      },
    },
    '& .MuiFormLabel-root': {
      fontFamily: 'roboto',
    },
    '& .MuiInputLabel-shrink': {
      fontSize: 0,
    },
  },
})(TextField);

export default memo(CssTextField);
