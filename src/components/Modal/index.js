import React from 'react';
import PropTypes from 'prop-types';
import Modal from '@material-ui/core/Modal';
import Fade from '@material-ui/core/Fade';
import CloseIcon from '@material-ui/icons/Close';
import { useStyles } from './styles';

const ModalMaterial = ({ open, close, content }) => {
  const classes = useStyles();

  return (
    <>
      <Modal
        open={open}
        aria-labelledby="Popup"
        aria-describedby="Popup"
        onClose={close}
        className={classes.modal}
        closeAfterTransition
      >
        <Fade in={open}>
          <div className={classes.paper}>
            <CloseIcon onClick={() => close()} className={classes.icon} />
            {content}
          </div>
        </Fade>
      </Modal>
    </>
  );
};

ModalMaterial.propTypes = {
  open: PropTypes.bool,
  close: PropTypes.func.isRequired,
  content: PropTypes.shape({}).isRequired,
};

ModalMaterial.defaultProps = {
  open: false,
};

export default ModalMaterial;
