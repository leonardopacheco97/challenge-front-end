import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
  paper: {
    backgroundColor: theme.palette.background.paper,
    border: 'none',
    padding: theme.spacing(2, 4, 3),
    maxWidth: '500px',
    borderRadius: '4px',
    outline: 'none',
  },
  icon: {
    float: 'right',
    cursor: 'pointer',
    fontSize: '15px',
  },
  modal: { display: 'flex', alignItems: 'center', justifyContent: 'center' },
}));

export { useStyles as defalut, useStyles };
