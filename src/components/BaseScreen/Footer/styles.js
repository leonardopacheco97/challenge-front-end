import { makeStyles } from '@material-ui/styles';

const useStyles = makeStyles(() => ({
  margin: {
    margin: '40px 0px',
  },
  marginBottom: {
    marginBottom: '24px',
  },
  icons: { marginRight: '16px', color: 'yellow' },
}));

export { useStyles as default, useStyles };
