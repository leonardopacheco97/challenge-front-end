import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { reduxForm } from 'redux-form';
import { Grid, Typography } from '@material-ui/core';
import { Facebook, Twitter, Pinterest } from '@material-ui/icons';
import Textfield from '../../Textfield/reduxField';
import { Button, Toast } from '../..';
import { useStyles } from './styles';

const Footer = ({ handleSubmit }) => {
  const classes = useStyles();

  const [openToast, setOpenToast] = useState(false);
  const [message, setMessage] = useState('');

  const handleOpenToast = () => {
    setOpenToast(true);
  };

  const handleCloseToast = () => {
    setOpenToast(false);
  };

  const onSubmit = ({ email }) => {
    setMessage(`An email has been sent to: ${email}`);
    handleOpenToast();
  };

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <Grid
        item
        container
        justify="center"
        alignItems="flex-start"
        style={{ backgroundColor: 'black', color: 'white' }}
      >
        <Grid item container xs={12} md={12} justify="center" style={{ marginTop: '40px' }}>
          <Grid item container xs={12} md={12} justify="center">
            <Typography variant="h4" component="h4" style={{ color: 'yellow' }}>
              Keep in touch with us
            </Typography>
          </Grid>
          <Grid item container xs={12} md={12} justify="center" className={classes.margin}>
            <Typography style={{ color: '#696969' }}>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent vitae eros eget
              tellus tristique bibendum. Donec rutrun sed sem quis veneratis
            </Typography>
          </Grid>
        </Grid>
        <Grid item xs={10} md={4}>
          <Textfield label="enter your email to update" name="email" />
        </Grid>
        <Grid item xs={3} md={1} style={{ marginLeft: '16px', marginTop: '8px' }}>
          <Button type="submit" text="submit" />
        </Grid>
        <Grid
          item
          container
          xs={12}
          md={12}
          justify="center"
          alignItems="center"
          className={classes.margin}
        >
          <Facebook className={classes.icons} fontSize="large" />
          <Twitter className={classes.icons} fontSize="large" />

          <Pinterest className={classes.icons} fontSize="large" />
        </Grid>
        <Grid item container xs={12} md={12} justify="center" className={classes.margin}>
          <Grid
            item
            container
            xs={12}
            sm={2}
            md={2}
            justify="center"
            className={classes.marginBottom}
          >
            <Typography>
              Alameda Santos, 1978
              <br />
              6th floot - jardim Paulista
              <br />
              São Paulo - SP
              <br />
              +55 11 30908500
            </Typography>
          </Grid>
          <Grid
            item
            container
            xs={12}
            sm={2}
            md={2}
            justify="center"
            // alignItems="center"
            className={classes.marginBottom}
          >
            <Typography>
              London - UK
              <br />
              125 Kingsway
              <br />
              London WC2B 6NH
            </Typography>
          </Grid>
          <Grid
            item
            container
            xs={12}
            sm={2}
            md={2}
            justify="center"
            className={classes.marginBottom}
          >
            <Typography>
              Lisbon - Portugual
              <br />
              Rua Rodrigues Faria, 103
              <br />
              4th floot
              <br />
              Lisbon - Portugual
            </Typography>
          </Grid>
          <Grid
            item
            container
            xs={12}
            sm={2}
            md={2}
            justify="center"
            className={classes.marginBottom}
          >
            <Typography>
              Curitiba - PR
              <br />
              R. Francisco Rocha, 198
              <br />
              Batel - Curitiba - PR
            </Typography>
          </Grid>
          <Grid
            item
            container
            xs={12}
            sm={2}
            md={2}
            justify="center"
            className={classes.marginBottom}
          >
            <Typography>
              Buenos Aires - Argentina
              <br />
              Esmeralda 950
              <br />
              Buenos Aires B C1007
            </Typography>
          </Grid>
        </Grid>
      </Grid>
      <Toast
        open={openToast}
        close={handleCloseToast}
        message={message}
        duration={3000}
        type="success"
      />
    </form>
  );
};

Footer.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
};

export default reduxForm({
  form: 'formEmail',
})(Footer);
