import React from 'react';

import AndroidOutlinedIcon from '@material-ui/icons/AndroidOutlined';
import { Apple } from '@material-ui/icons';
import { Navigator, Slider, Container } from './styles';
import Logo from '../../../assets/images/logo.png';
import Tablet from '../../../assets/images/Tablet.png';

const Header = () => {
  return (
    <Container>
      <Navigator>
        <img src={Logo} alt="Pixter Books Logo" />
        <nav>
          <a href="#books">Books</a>
          <a href="#newsletter">Newsletter</a>
          <a href="#address">Address</a>
        </nav>
      </Navigator>

      <Slider>
        <div className="content">
          <div className="left-side">
            <div>
              <h1>Pixter Digital Books</h1>
              <h2>Lorem ipsum dolor sit amet? consectetur elit, volutpat.</h2>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent vitae eros eget
                tellus tristique bibendum. Donec rutrum sed sem quis venenatis. Proin viverra risus
                a eros volutpat tempor. In quis arcu et eros porta lobortis sit
              </p>
              <div className="icons">
                <Apple fontSize="large" />
                <AndroidOutlinedIcon fontSize="large" />
                {/* <FaWindows size={35} /> */}
              </div>
            </div>
          </div>
          <div className="right-side">
            <img src={Tablet} alt="Mockup" />
          </div>
        </div>
        <div className="dots">
          <div className="dot active" />
          <div className="dot" />
          <div className="dot" />
        </div>
      </Slider>
    </Container>
  );
};

export default Header;
