import styled from 'styled-components';

const Navigator = styled.div`
  height: 2.75rem;
  margin-bottom: 4rem;

  display: flex;
  justify-content: space-between;
  align-items: baseline;

  nav a {
    font-size: 22px;
    font-weight: bold;
    text-decoration: none;
    color: 'black';

    &:not(:first-of-type) {
      margin-left: 5rem;
    }
  }
  a {
    color: black;
  }

  @media screen and (max-width: 800px) {
    flex-direction: column;
    align-items: center;

    img {
      margin-bottom: 1rem;
    }

    nav a:not(:first-of-type) {
      margin-left: 1rem;
    }
  }

  @media screen and (max-width: 1100px) {
    img {
      margin-bottom: 1rem;
    }

    nav a:not(:first-of-type) {
      margin-left: 1rem;
    }
  }
`;

const Container = styled.section`
  height: 100%;
  padding: 3.25rem 15rem;

  background-color: #fcdb00;

  @media screen and (max-width: 900px) {
    padding: 3.25rem 5rem;
  }
`;

const Slider = styled.div`
  height: 100%;

  display: flex;
  flex-direction: column;
  align-items: center;

  .content {
    width: 100%;

    display: flex;
    align-items: center;
    justify-content: space-around;
    margin-bottom: 2.75rem;

    .left-side {
      flex: 1;

      div {
        & > * {
          margin-bottom: 1.69rem;
        }

        h1 {
          font-size: 32px;
        }

        h2 {
          width: 15.25rem;

          color: #313841;
          font-size: 20px;
        }

        p {
          width: 25rem;

          color: #555555;
          font-size: 16px;
        }
      }

      .icons *:not(:last-of-type) {
        margin-right: 2.37rem;
      }
    }

    .right-side {
      flex: 1;

      display: flex;
      justify-content: center;
    }
  }

  .dots {
    display: flex;

    .dot {
      width: 1rem;
      height: 1rem;

      background-color: #ffffffa1;
      border-radius: 50%;

      &:not(:last-of-type) {
        margin-right: 0.5rem;
      }

      &.active {
        background-color: #ffffff;
      }
    }
  }

  @media screen and (max-width: 800px) {
    .content {
      flex-direction: column;

      .left-side {
        div {
          p {
            width: 20rem;
          }

          .icons {
            display: flex;
            justify-content: space-around;
          }
        }
      }
    }
  }
`;

export { Navigator, Slider, Container };
