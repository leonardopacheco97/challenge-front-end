import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles({
  root: {
    // display: 'flex',
    maxWidth: '125px',
    boxSizing: 'border-box',
    margin: 16,
  },
  media: {
    height: 180,
    width: 120,
    display: 'flex',
  },
});

export { useStyles as default, useStyles };
